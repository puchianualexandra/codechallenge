﻿namespace CodeChallenge.DataLayer.Entities
{
    public class Customer
    {
        public int id { get; set; }

        public string first_name { get; set; }
        
        public string last_name { get; set; }
        
        public string address_name { get; set; }

        public int age { get; set; }
        
        public int points { get; set; }
        
        public bool receives_newsletters { get; set; }
    }
}

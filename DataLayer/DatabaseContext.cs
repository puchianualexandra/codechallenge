﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using CodeChallenge.DataLayer.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;

namespace CodeChallenge.DataLayer
{
    public class DatabaseContext :DbContext
    {
        IConfiguration configuration;

        public DatabaseContext(IConfiguration configuration)
        {
            this.configuration = configuration;
        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            string connectionString = "Server=(LocalDb)\\MSSQLLocalDB;Database=CodeChallenge;Trusted_connection=true";
            optionsBuilder
                .UseSqlServer(connectionString, providerOptions => { providerOptions.EnableRetryOnFailure(); });
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Customer>().HasKey(u => u.id);
        }

        public DbSet<Customer> customer { get; set; }
    }
}

﻿using System;
using System.Threading.Tasks;
using Abp.Extensions;
using CodeChallenge.Models.DTOs;
using CodeChallenge.Services;
using Microsoft.AspNetCore.Mvc;

namespace CodeChallenge.Controllers
{
    [ApiController]
    [Route("customers")]
    public class CustomersController : ControllerBase
    {
        CustomersService customersService { get; set; }

        public CustomersController(CustomersService customersService)
        {
            this.customersService = customersService;
        }


        [HttpGet]
        public IActionResult GetAll()
        {
            var results = customersService.GetAll();

            if (results == null)
                return BadRequest();

            return Ok(results);
        }

        [HttpPost]
        public IActionResult Post([FromBody] CustomerDto customerDto)
        {
            var response = customersService.AddAsync(customerDto);

            return Ok(new {response });
        }

        [Route("import-data")]
        [HttpPost]
        public async Task<IActionResult> ImportAsync([FromBody] string path)
        {
            if (path.IsNullOrEmpty() || path.IsNullOrWhiteSpace())
            {
                return BadRequest("File path could not be null.");
            }

            try
            {
                await customersService.ImportCsvFileAsync(path);
                return Ok("Csv file imported successfully.");
            }
            catch (Exception e)
            {
                return Problem(e.Message);
            }
        }
    }
}

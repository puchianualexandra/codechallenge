﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CodeChallenge.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "dbo.customer",
                columns: table => new
                {
                    first_name = table.Column<string>(type: "nvarchar(1024)", nullable: false),
                    last_name = table.Column<string>(type: "nvarchar(1024)", nullable: false),
                    address_name = table.Column<string>(type: "nvarchar(1024)", nullable: false),
                    age = table.Column<int>(type: "int", nullable: false),
                    points = table.Column<int>(type: "int", nullable: false),
                    receives_newsletters = table.Column<byte>(type: "tinyint", nullable: false)
                },
                constraints: table =>
                {
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "dbo.customer");
        }
    }
}

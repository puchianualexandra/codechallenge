﻿using System;
using CodeChallenge.Models.DTOs;
using CodeChallenge.Repositories;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using CodeChallenge.DataLayer.Entities;
using CodeChallenge.Mappers;
using CsvHelper;
using System.Globalization;
using System.Threading.Tasks;
using Abp.UI;
using Castle.Core.Internal;
using System.Diagnostics;
using System.Data;
using System.Data.SqlClient;

namespace CodeChallenge.Services
{
    public class CustomersService
    {
        CustomersRepository customersRepository { get; set; }
        private readonly string connectionString;
        private readonly string destinationTableName;
        private readonly List<string> headerColumns;

        public CustomersService(CustomersRepository customersRepository)
        {
            this.customersRepository = customersRepository;
            connectionString = "Server=(LocalDb)\\MSSQLLocalDB;Database=CodeChallenge;Trusted_connection=true";
            destinationTableName = "customer";
            headerColumns = new List<string> {"first_name","last_name","address_name","age","points","receives_newsletters"};
        }

        public List<CustomerDto> GetAll()
        {
            var results = customersRepository.GetAll();
            var listCustomerDtos = new List<CustomerDto>();

            foreach (var customer in results)
            {
                listCustomerDtos.Add(CustomerMap.AdaptToDto(customer));
            }

            return listCustomerDtos;
        }


        public async Task<string> AddAsync(CustomerDto customer)
        {
            var newCustomer = CustomerMap.AdaptToModel(customer);
            await customersRepository.AddAsync(newCustomer);

            return "Success!";
        }


        public static int GetNumericValue(object a)
        {
            if (a is "false")
                return 0;
            return 1;
        }
        
        public async Task ImportCsvFileAsync(string path)
        {
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();
            try
            {
                using (var reader = new StreamReader(path))
                {
                    using (var csvReader = new CsvReader(reader, CultureInfo.InvariantCulture))
                    {
                        var chunkSize = 0;
                        var records = new List<Customer>();

                        using (var sqlBulk = new SqlBulkCopy(connectionString))
                        {
                            var table = new DataTable();
                            sqlBulk.ColumnMappings.Clear();

                            foreach (var c in headerColumns)
                            {
                                table.Columns.Add(c);
                                sqlBulk.ColumnMappings.Add(c, c);
                            }

                            sqlBulk.DestinationTableName = destinationTableName;

                            foreach (IDictionary<string, object> customer in csvReader.GetRecords<dynamic>())
                            {
                                var rowValues = customer.Values
                                        .Select(a => string.IsNullOrEmpty(a.ToString()) ? null : (a is "true" or "false" ? GetNumericValue(a) : a))
                                        .ToArray();

                                table.Rows.Add(rowValues);
                                if (chunkSize < 50000)
                                {
                                    chunkSize++;
                                }
                                else
                                {
                                    await sqlBulk.WriteToServerAsync(table);
                                    table.Rows.Clear();
                                    chunkSize = 0;
                                }
                            }

                            if (!table.Rows.IsNullOrEmpty())
                            {
                                await sqlBulk.WriteToServerAsync(table);
                            }

                        }
                    }
                }

                stopwatch.Stop();
                Console.WriteLine("Elapsed Time is {0} ms", stopwatch.ElapsedMilliseconds);
            }
            catch (Exception e)
            {
                stopwatch.Stop();
                Console.WriteLine("Elapsed Time is {0} ms", stopwatch.ElapsedMilliseconds);
                throw new UserFriendlyException(e.Message);
            }
        }
    }
}

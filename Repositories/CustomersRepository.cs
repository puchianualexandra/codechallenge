﻿using System;
using CodeChallenge.DataLayer;
using CodeChallenge.DataLayer.Entities;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EFCore.BulkExtensions;
using Microsoft.EntityFrameworkCore;

namespace CodeChallenge.Repositories
{
    public class CustomersRepository
    {
        DatabaseContext databaseContext { get; set; }

        public CustomersRepository(DatabaseContext databaseContext)
        {
            this.databaseContext = databaseContext;
        }

        public List<Customer> GetAll()
        {
            var results = databaseContext.customer
                .ToList();

            return results;
        }

        public Customer GetById(int id)
        {
            var customer = databaseContext.Find<Customer>(id);
            return customer;
        }
        

        public async Task AddAsync(Customer customer)
        {
            await databaseContext.customer.AddAsync(customer);
        }
        public async Task BulkInsertAsync(List<Customer> customers)
        {
            await databaseContext.BulkInsertAsync(customers);
        }

        public async Task<int> SaveContextAsync()
        {
            return await databaseContext.SaveChangesAsync();
        }

        public void BulkInsert(List<Customer> customers)
        {
            var bulkConfig = new BulkConfig { PreserveInsertOrder = true, SetOutputIdentity = true };

            using var transaction = databaseContext.Database.BeginTransaction();
            databaseContext.BulkInsert(customers, bulkConfig);
            Console.WriteLine("Add customers from Csv file");

            transaction.Commit();
        }



        public void Delete(Customer customer)
        {
            databaseContext.Remove<Customer>(customer);
            databaseContext.SaveChanges();
        }
    }
}

﻿using System;
using CodeChallenge.DataLayer.Entities;
using CodeChallenge.Models.DTOs;
using CsvHelper.Configuration;

namespace CodeChallenge.Mappers
{
    public class CustomerMap : ClassMap<Customer>
    {
        public CustomerMap()
        {
            Map(x => x.first_name).Name("first_name");
            Map(x => x.last_name).Name("last_name");
            Map(x => x.address_name).Name("address");
            Map(x => x.age).Name("age");
            Map(x => x.points).Name("points");
            Map(x => x.receives_newsletters).Name("receives_newsletters");
        }

        public static CustomerDto AdaptToDto(Customer source)
        {
            if (source == null)
            {
                return null;
            }

            var customer = new CustomerDto()
            {
                FirstName = source.first_name,
                LastName = source.last_name,
                AddressName = source.address_name,
                Age = source.age,
                Points = source.points,
                ReceivesNewsletters = source.receives_newsletters
            };

            return customer;
        }

        public static Customer AdaptToModel(CustomerDto source)
        {
            if (source == null)
            {
                return null;
            }

            var customer = new Customer()
            {
                first_name = source.FirstName,
                last_name = source.LastName,
                address_name = source.AddressName,
                age = source.Age,
                points = source.Points,
                receives_newsletters = source.ReceivesNewsletters
            };

            return customer;
        }
    }
}
﻿using CsvHelper.Configuration.Attributes;

namespace CodeChallenge.Models.DTOs
{
    public class CustomerDto
    {

        [Name("first_name")]
        public string FirstName { get; set; }

        [Name("last_name")]
        public string LastName { get; set; }

        [Name("address")]
        public string AddressName { get; set; }

        [Name("age")]
        public int Age { get; set; }

        [Name("points")]
        public int Points { get; set; }

        [Name("receives_newsletters")]
        public bool ReceivesNewsletters { get; set; }
    }
}
